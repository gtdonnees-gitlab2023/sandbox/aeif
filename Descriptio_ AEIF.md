
Cette association a pour objet :

l’étude des questions intéressant l’enseignement de l’informatique ;
la défense des intérêts professionnels de ses adhérents ;
le développement des échanges entre les personnes et les établissements concernant l’informatique et son enseignement ;
l’aide à la formation initiale et continue des enseignants en informatique.
Elle engage ou soutient toute action qui lui paraît propre à améliorer l’enseignement de l’informatique.

Elle diffuse à l’intention de ses adhérents tout document qu’elle juge utile. Elle peut publier un bulletin.

Elle gère la forge AEIF
https://forge.aeif.fr/explore

« Que la forge soit avec toi… »
Envisagée comme un commun numérique, le projet de Forge des Communs Numériques Éducatifs FCNE a pour objectif de mettre à disposition des enseignants une forge GitLab en gouvernance partagée entre le ministère et la communauté de ses utilisateurs.

